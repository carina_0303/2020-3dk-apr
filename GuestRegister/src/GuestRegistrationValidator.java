import java.util.ArrayList;

// AVK3: 10 Punkte

// wird nirgendwo verwendet: -2
public class GuestRegistrationValidator {


    // Methode sollte validate heißen (siehe Angabe)
    public void GuestRegistrationValidator(String textFieldVn,String textFieldNn, String textFieldEmail,String textFieldTelephone, String textFieldTableNumber)throws GuestDataNotValidException{

        ArrayList<String> exceptions = new ArrayList<>();
        boolean errored = false;

        try {
            checkEmptyAndLength(textFieldVn,15,"\nFirst");
        } catch (IllegalArgumentException e) {
            exceptions.add(e.toString());
            errored = true;
        }

        try {
            checkEmptyAndLength(textFieldNn,20,"Second");
        } catch (IllegalArgumentException e) {
            exceptions.add(e.getMessage());
            errored = true;
        }

        try {
            checkMaxMustermann(textFieldVn,textFieldNn);
        } catch (IllegalArgumentException e) {
            exceptions.add(e.getMessage());
            errored = true;
        }

        try {
            checkTableNumber(textFieldTableNumber);
        } catch (IllegalArgumentException e) {
            exceptions.add(e.getMessage());
            errored = true;
        }

        try {
            checkEmailTelephoneNumber(textFieldEmail,textFieldTelephone);
        } catch (IllegalArgumentException e) {
            exceptions.add(e.getMessage());
            errored = true;
        }
        if(errored){
            throw new GuestDataNotValidException(exceptions);
        }
    }

    // Gute Idee mit dem dritten Parameter
    private void checkEmptyAndLength (String value, int number, String name){
        if(value.isEmpty() || value.length()>number){
            throw new IllegalArgumentException(name + " name must not be empty or longer than "+number+ " words");
        }
    }

    private void checkMaxMustermann(String textFieldVn, String textFieldNn){
        if(textFieldVn.equals("Max") && textFieldNn.equals("Mustermann")){
            throw new IllegalArgumentException("Max Mustermann is not allowed");
        }
    }
    private void checkTableNumber (String textFieldTableNumber){
        try {
            int value = Integer.parseInt(textFieldTableNumber);
            if(value <1 || value > 100 ){
                throw new IllegalArgumentException("Table number must not be empty");

            }

        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Table number must not be empty");

        }

    }
    private void checkEmailTelephoneNumber (String textFieldEmail, String textFieldTelephone){
        if(textFieldEmail.isEmpty() && textFieldTelephone.isEmpty()){
            throw new IllegalArgumentException("Email or phone number must be provided");
        }

    }


}
