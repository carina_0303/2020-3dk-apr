import java.util.ArrayList;

public class GuestDataNotValidException extends Exception{

    private ArrayList<String> exceptions;

    public GuestDataNotValidException(ArrayList<String>exceptions){
        this.exceptions = exceptions;

    }

    public ArrayList<String> getExceptions() {
        return exceptions;

    }
}
