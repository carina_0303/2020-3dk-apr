import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GaesteregistrierungFields extends JPanel {

    private JTextField textFieldVn = new JTextField();
    private JTextField textFieldNn = new JTextField();
    private JTextField textFieldStreet = new JTextField();
    private JTextField textFieldOrt = new JTextField();
    private JTextField textFieldPlz = new JTextField();
    private JTextField textFieldTelephone = new JTextField();
    private JTextField textFieldEmail = new JTextField();
    private JTextField textFieldTableNumber = new JTextField();

    private JButton save = new JButton("Speichern");



    private JButton reset = new JButton("Zurücksetzen");


    public GaesteregistrierungFields() {
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GuestRegistrationValidator guestRegistrationValidator = new GuestRegistrationValidator();
                try {
                    guestRegistrationValidator.GuestRegistrationValidator(textFieldVn.getText()
                            ,textFieldNn.getText()
                            ,textFieldEmail.getText()
                            ,textFieldTelephone.getText()
                            ,textFieldTableNumber.getText());
                } catch (GuestDataNotValidException guestDataNotValidException) {
                    for (int i = 0; i < guestDataNotValidException.getExceptions().size(); i++) {
                        System.err.println(guestDataNotValidException.getExceptions().get(i));
                    }
                    
                }
            }
        });
        setPreferredSize(new Dimension(550,255));
        setLayout(null);
        setBorder(new LineBorder(Color.RED,1));

        JLabel vorname = new JLabel("Vorname:");
        JLabel nachname = new JLabel("Nachname:");
        JLabel strasse = new JLabel("Straße:");
        JLabel plz = new JLabel("PLZ:");
        JLabel ort = new JLabel("Ort:");
        JLabel telnum = new JLabel("Telefonnummer:");
        JLabel email = new JLabel("E-Mail:");
        JLabel tischnr = new JLabel("Tisch-Nr.:");

        vorname.setBounds(20,0,100,25);
        nachname.setBounds(20,25,100,25);
        strasse.setBounds(20,50,100,25);
        plz.setBounds(20,75,100,25);
        ort.setBounds(20,100,100,25);
        telnum.setBounds(300,0,100,25);
        email.setBounds(300,25,100,25);
        tischnr.setBounds(300,50,100,25);

        add(vorname);
        add(nachname);
        add(strasse);
        add(plz);
        add(ort);
        add(telnum);
        add(email);
        add(tischnr);

        textFieldVn.setBounds(120,5,100,20);
        textFieldNn.setBounds(120,27,100,20);
        textFieldStreet.setBounds(120,49,100,20);
        textFieldOrt.setBounds(120,73,100,20);
        textFieldPlz.setBounds(120,95,100,20);
        textFieldTelephone.setBounds(400,5,100,20);
        textFieldEmail.setBounds(400,27,100,20);
        textFieldTableNumber.setBounds(400,49,100,20);

        add(textFieldVn);
        add(textFieldNn);
        add(textFieldStreet);
        add(textFieldOrt);
        add(textFieldPlz);
        add(textFieldTelephone);
        add(textFieldEmail);
        add(textFieldTableNumber);

        save.setBounds(150,200,150,25);
        reset.setBounds(300,200,150,25);

        add(save);

        add(reset);

    }
}
