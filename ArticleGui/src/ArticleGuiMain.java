import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class ArticleGuiMain {

    public static void main(String[] args) throws IOException {

        Properties properties= new Properties();
        FileInputStream in = new FileInputStream("config.properties.article.gui");
        properties.load(in);
        in.close();

        String user = (String) properties.get("user");
        String password = (String) properties.get("password");
        String host = (String) properties.get("host");
        String port = (String) properties.get("port");
        String database = (String) properties.get("database");

        new ArticleDataAccessObject(host,port,database,user,password);

        new ArticleGui();

    }
}



