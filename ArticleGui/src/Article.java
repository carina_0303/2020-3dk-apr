public class Article {
    private final int articleId;
    private final String articleDes;
    private final double articlePrice;
    private final int stock;


    public String getArticleDes() {
        return articleDes;
    }

    public double getArticlePrice() {
        return articlePrice;
    }

    public int getStock() {
        return stock;
    }

    public Article(int articleId, String articleDes, double articlePrice, int stock) {
        this.articleId = articleId;
        this.articleDes = articleDes;
        this.articlePrice = articlePrice;
        this.stock = stock;
    }

    public int getArticleId() {
        return articleId;
    }

    @Override
    public String toString() {
        return articleId + " | " + articleDes + " | " + articlePrice + " | " + stock;
    }
}

