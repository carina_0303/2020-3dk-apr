import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ArticleListSelectionListener implements ListSelectionListener {

    private JTable table;
    private PanelA panelA;

    public ArticleListSelectionListener(JTable table, PanelA panelA){
        this.table = table;
        this.panelA = panelA;
    }
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            return;
        }
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        int selectedRow = lsm.getMinSelectionIndex();
        if(selectedRow >= 0){
            int id = (int) table.getValueAt(selectedRow,0);
            panelA.loadArticle(id);
        }

    }
}
