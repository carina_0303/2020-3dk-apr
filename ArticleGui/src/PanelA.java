import com.sun.jdi.IntegerType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;

public class PanelA extends JPanel {

    private final ArticleDataAccessObject articleDao;

    private JTextField articleNumberTextField;
    private JTextField articleDescTextField;
    private JTextField priceTextField;
    private JTextField stockTextField;

    public PanelA(ArticleDataAccessObject articleDao, ArticleTableModel tableModel) {
        super();
        setLayout(null);
        ArticleDatabase database = new ArticleDatabase();
        
        this.articleDao = articleDao;



        JLabel articleLabel = new JLabel("Artikel");
        articleLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
        articleLabel.setBounds(20, 10, 300, 30);
        add(articleLabel);

        JButton newButton = new JButton("Neu");
        newButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetFields();
            }
        });
        newButton.setBounds(330, 10, 100, 20);
        add(newButton);

        // next row
        JLabel articleNumberLabel = new JLabel("Artikelnummer");
        articleNumberLabel.setBounds(20, 50, 130, 20);
        add(articleNumberLabel);


        articleNumberTextField = new JTextField();
        articleNumberTextField.setEnabled(false);
        articleNumberTextField.setBounds(150, 50, 170, 20);
        add(articleNumberTextField);


        JButton anlegenButton = new JButton("Anlegen");
        anlegenButton.setBounds(330, 50, 100, 20);
        add(anlegenButton);


        // next row
        JLabel articleDescLabel = new JLabel("Artikelbezeichnung");
        articleDescLabel.setBounds(20, 75, 130, 20);
        add(articleDescLabel);

        articleDescTextField = new JTextField();
        articleDescTextField.setBounds(150, 75, 170, 20);
        add(articleDescTextField);

        JButton aendernButton = new JButton("Ändern");
        aendernButton.setBounds(330, 75, 100, 20);
        aendernButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(articleNumberTextField.getText());
                String articleDes = checkEmpty(articleDescTextField.getText());
                articleDes = checkMaxLength(articleDes,100);
                double articlePrice = checkDouble(priceTextField.getText());
                int stock = checkInteger(stockTextField.getText());
                Article a = new Article(id, articleDes,articlePrice,stock);
                articleDao.update(a);
                List<Article> articles = articleDao.readAllArticles();
                articles.forEach(System.out::println);
                tableModel.initData(articles);

            }
        });
        add(aendernButton);


        // next row
        JLabel priceLabel = new JLabel("Verkaufspreis");
        priceLabel.setBounds(20, 100, 130, 20);
        add(priceLabel);

        priceTextField = new JTextField();
        priceTextField.setBounds(150, 100, 170, 20);
        add(priceTextField);

        JButton loeschenButton = new JButton("Löschen");
        loeschenButton.setBounds(330, 100, 100, 20);
        add(loeschenButton);
        loeschenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int articleId = checkInteger(articleNumberTextField.getText());
                articleDao.delete(articleId);
                List<Article> articles = articleDao.readAllArticles();
                articles.forEach(System.out::println);
                tableModel.initData(articles);
                resetFields();
            }
        });

        // next row
        JLabel stockLabel = new JLabel("Bestand");
        stockLabel.setBounds(20, 125, 130, 20);
        add(stockLabel);

        stockTextField = new JTextField();
        stockTextField.setBounds(150, 125, 170, 20);
        add(stockTextField);

        JButton suchenButton = new JButton("suchen");
        suchenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String desc = articleDescTextField.getText();
                List <Article> foundArticles = articleDao.findBy(desc);
                tableModel.initData(foundArticles);

            }
        });
        suchenButton.setBounds(330, 125, 100, 20);
        add(suchenButton);



        anlegenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String articleDes = checkEmpty(articleDescTextField.getText());
                articleDes = checkMaxLength(articleDes,100);
                double articlePrice = checkDouble(priceTextField.getText());
                int stock = checkInteger(stockTextField.getText());

                articleDao.insert(articleDes,articlePrice,stock);
                List<Article> articles = articleDao.readAllArticles();
                articles.forEach(System.out::println);
                tableModel.initData(articles);
            }


        });
        setPreferredSize(new Dimension(500, 200));

    }
    private void resetFields(){
        this.articleNumberTextField.setText("");
        this.articleDescTextField.setText("");
        this.priceTextField.setText("");
        this.stockTextField.setText("");
    }

    private String checkEmpty(String value) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("Eingabe darf nicht leer sein");
        }
        return value;
    }

    private String checkMaxLength(String value, int maxLength) {
        if (value.length() > maxLength) {
            throw new IllegalArgumentException("Die Beschreibung darf nicht länger als 15 Zeichen lang sein");
        }
        return value;
    }

    private int checkInteger(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Die Eingabe muss eine ganze Zahl sein");

        }
    }

    private double checkDouble (String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Die Eingabe darf nur Ganzzahlig oder eine Dezimalzahl sein");
        }
    }
    public void loadArticle(int id){
        Article article = articleDao.read(id);
        articleNumberTextField.setText(article.getArticleId()+"");
        articleDescTextField.setText(article.getArticleDes());
        priceTextField.setText(article.getArticlePrice()+"");
        stockTextField.setText(article.getStock()+"");
    }


}
