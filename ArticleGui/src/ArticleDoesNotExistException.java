public class ArticleDoesNotExistException extends RuntimeException{

    public ArticleDoesNotExistException(int articleId){
        super("ARticle with id "+articleId+" does not exst");
    }
}
