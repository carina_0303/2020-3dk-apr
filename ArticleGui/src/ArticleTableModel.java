import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ArticleTableModel extends AbstractTableModel {

    private final String [] columnNames = {"Id", "Description","Price","Stock"};

    private List<Article> articles;

    public void initData(List<Article> articles){
        this.articles = articles;
        fireTableDataChanged();
    }


    @Override
    public int getRowCount() {
        if(articles != null){
            return articles.size();
        }
        return 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Article article = articles.get(rowIndex);
        switch (columnIndex){
            case 0:
                return article.getArticleId();
            case 1:
                return article.getArticleDes();
            case 2:
                return article.getArticlePrice();
            case 3:
                return article.getStock();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
}
