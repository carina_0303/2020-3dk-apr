import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class PanelB extends JPanel {

    public PanelB(ArticleDataAccessObject articleDao,ArticleTableModel tableModel, PanelA panelA) {

        setLayout(new BorderLayout());

        JTable table = new JTable(tableModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSelectionModel = table.getSelectionModel();
        rowSelectionModel.addListSelectionListener(new ArticleListSelectionListener(table, panelA));

        JScrollPane jScrollPane = new JScrollPane(table);
        jScrollPane.setPreferredSize(new Dimension(400, 120));
        add(jScrollPane, BorderLayout.NORTH);

        JButton showArticleListButton = new JButton("Zeige Artikelliste");
        showArticleListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Article> articles = articleDao.readAllArticles();
                tableModel.initData(articles);
            }
        });
        showArticleListButton.setPreferredSize(new Dimension(100, 20));
        add(showArticleListButton, BorderLayout.WEST);

        JButton beendenButton = new JButton("Beenden");
        beendenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        beendenButton.setPreferredSize(new Dimension(100, 20));
        add(beendenButton, BorderLayout.EAST);

        setPreferredSize(new Dimension(400, 140));

    }

}
