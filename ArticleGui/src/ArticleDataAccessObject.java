import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ArticleDataAccessObject {

    private final String user;

    private final String password;

    private final String databaseUrl;

    public ArticleDataAccessObject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.databaseUrl = "jdbc:mariadb://" + host + ":" + port + "/" + database;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not lad database driver", e);
        }
    }
    public void update (Article article){
        Connection connection = connect();
        String sql = "update article set description = '"+article.getArticleDes()+"', price = '"+article.getArticlePrice()+"',stock = '"+article.getStock()+"'where id = '"+article.getArticleId()+"'";
        Statement update = null;

        try {
            update = connection.createStatement();
            update.execute(sql);
            System.out.println("Update executed successfully");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            close(connection);
        }

    }
    public void delete (int id){
        Connection connection = connect();
        Statement delete = null;
        String sql = "delete from article where id = "+id;
        try {
            delete = connection.createStatement();
            delete.execute(sql);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }
    public List<Article> findBy (String description){
        Connection connection = connect();
        String sql = "Select * from article where description like '%"+description+"%'";
        Statement findBy = null;
        try {
            findBy = connection.createStatement();
            ResultSet resultSet = findBy.executeQuery(sql);
            extractArticles(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            close(connection);
        }
        return new ArrayList<>();

    }

    private List<Article> extractArticles(ResultSet resultSet) throws SQLException {
        ArrayList<Article> articles = new ArrayList<>();
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String desc = resultSet.getString(2);
            Double prc = resultSet.getDouble(3);
            int stck = resultSet.getInt(4);
            articles.add(new Article(id, desc, prc, stck));
        }
        return articles;
    }

    public Article read(int id) {
        Connection connection = connect();
        String sql = "select * from article where id = " + id;
        Statement select = null;
        try {
            select = connection.createStatement();
            ResultSet resultSet = select.executeQuery(sql);
            List<Article> articles = extractArticles(resultSet);
            return articles.get(0);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create article", throwables);
        } finally {
            close(connection);
        }
    }




    public List<Article> readAllArticles() {
        Connection connection = connect();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String sql = "select * from article";
            ResultSet resultSet = statement.executeQuery(sql);
            return extractArticles(resultSet);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create article", throwables);
        } finally {
            close(connection);
        }
    }

    public void insert(String desc, double prc,int stck) {
        Connection connection = connect();
        String sql = "insert into article(description, price, stock) VALUES ('"+ desc + "' , '" + prc + "','" + stck+ "')";
        Statement insert = null;
        try {
            insert = connection.createStatement();
            insert.execute(sql);

        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        }finally {
            close(connection);
        }
    }


    private Connection connect(){
        try {
            return DriverManager.getConnection(this.databaseUrl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
