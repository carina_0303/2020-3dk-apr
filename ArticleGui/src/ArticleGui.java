import javax.swing.*;
import java.awt.*;

public class ArticleGui extends JFrame {

    public ArticleGui() {
        super("Artikelverwaltung");

        JPanel contentPane = new JPanel();

        ArticleDataAccessObject articleDao = new ArticleDataAccessObject("localhost", "3306", "article_management", "root","");

        ArticleTableModel tableModel = new ArticleTableModel();
        tableModel.initData(articleDao.readAllArticles());
        PanelA panelA = new PanelA(articleDao,tableModel);
        contentPane.add(panelA);
        contentPane.add(new PanelB(articleDao,tableModel, panelA));

        setContentPane(contentPane);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));
        setVisible(true);
        pack();
    }

}
