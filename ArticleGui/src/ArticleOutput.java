import java.sql.*;
import java.util.ArrayList;

public class ArticleOutput {

    public ArticleOutput(){
    Connection connection = null;
        try {
        Class.forName("org.mariadb.jdbc.Driver");

        connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/article_management","root","");
        System.out.println("Connected to database successfully");



            readAll(connection);

        } catch (ClassNotFoundException | SQLException e) {
        e.printStackTrace();
    }finally {
        try {
            if(connection != null){
                connection.close();
            }
            connection.close();
            System.out.println("Connection closed");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}

    private void readAll(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        String article = "SELECT * from article";
        ResultSet resultSet = statement.executeQuery(article);
        ArrayList<Article> articles = new ArrayList<>();
        while(resultSet.next()){
            int id = resultSet.getInt(1);
            String des = resultSet.getString(2);
            double prc = resultSet.getDouble(3);
            int stk = resultSet.getInt(4);
            articles.add(new Article(id,des,prc,stk));



        }
        for(Article arcl : articles){
            System.out.println(arcl.toString());
        }
    }

}
