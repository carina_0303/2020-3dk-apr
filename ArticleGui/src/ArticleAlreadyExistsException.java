public class ArticleAlreadyExistsException extends Exception{

    public ArticleAlreadyExistsException(int articleId){
        super("Article with id "+ articleId + "already exists");
    }

}
