import java.util.HashMap;

public class ArticleDatabase {

    private HashMap<Integer,Article> database;

    public ArticleDatabase() {
        this.database = new HashMap<>();
    }



    public void insert(Article article) throws ArticleAlreadyExistsException {
        if(database.containsKey(article.getArticleId())){
            throw new ArticleAlreadyExistsException(article.getArticleId());
        }
        this.database.put(article.getArticleId(), article);
    }



    public void delete(int articleId){
        if(database.containsKey(articleId)){
            this.database.remove(articleId);
        }else{
            throw new ArticleDoesNotExistException(articleId);
        }

    }

    @Override
    public String toString() {
        return "ArticleDatabase{" +
                "database=" + database +
                '}';
    }
}
