import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModeSettingActionListener implements ActionListener {

    private final GeometricFiguresGui geometricFiguresGui;

    public ModeSettingActionListener(GeometricFiguresGui geometricFiguresGui) {
        this.geometricFiguresGui = geometricFiguresGui;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
        geometricFiguresGui.setMode(e.getActionCommand());

    }
}
