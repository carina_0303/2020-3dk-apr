import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseClickHandler extends MouseAdapter {

    private Canvas canvas;

    public MouseClickHandler(Canvas canvas){
        this.canvas = canvas;
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        canvas.setxPos(e.getX());
        canvas.setyPos(e.getY());
        canvas.repaint();
    }
}
