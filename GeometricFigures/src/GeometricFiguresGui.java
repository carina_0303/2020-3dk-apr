import javax.swing.*;
import java.awt.*;

public class GeometricFiguresGui extends JFrame {

    private String mode;

    public GeometricFiguresGui() {
        super("Geometrische Figuren zeichnen");

        Canvas drawingArea = new Canvas(this);
        drawingArea.addMouseListener(new MouseClickHandler(drawingArea));

        JPanel radioButtonPanel = new JPanel();
        radioButtonPanel.setPreferredSize(new Dimension(75,550));


        JRadioButton rectangleButton = new JRadioButton("Rechteck");
        rectangleButton.addActionListener(new ModeSettingActionListener(this));
        JRadioButton circleButton = new JRadioButton("Kreis");
        circleButton.addActionListener(new ModeSettingActionListener(this));
        JRadioButton discButton = new JRadioButton("Scheibe");
        discButton.addActionListener(new ModeSettingActionListener(this));

        ButtonGroup buttonGroup = new ButtonGroup();

        buttonGroup.add(rectangleButton);
        buttonGroup.add(circleButton);
        buttonGroup.add(discButton);

        radioButtonPanel.add(rectangleButton);
        radioButtonPanel.add(circleButton);
        radioButtonPanel.add(discButton);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new FlowLayout());
        contentPane.setSize(600,550);

        contentPane.add(drawingArea);
        contentPane.add(radioButtonPanel);

        setContentPane(contentPane);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,600);
        setVisible(true);

        pack();
    }

    public String getMode() {
        return mode;
    }
    public void setMode(String actionCommand) {
        this.mode = mode;
    }
}
