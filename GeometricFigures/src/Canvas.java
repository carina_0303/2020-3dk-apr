import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {

    private int xPos;
    private int yPos;

    private GeometricFiguresGui geometricFiguresGui;

    public Canvas(GeometricFiguresGui geometricFiguresGui) {

        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(500,550));
        this.geometricFiguresGui = geometricFiguresGui;


    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        setForeground(Color.RED);
        if("Rechteck".equals(geometricFiguresGui.getMode())){
            g.drawRect(xPos,yPos,100,80);
        }else if ("Kreis".equals(geometricFiguresGui.getMode())){
           g.drawOval(xPos,yPos,100,100);
        }else if ("Scheibe".equals(geometricFiguresGui.getMode())){
           g.fillOval(xPos,yPos,100,100);
        }

    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }
}

