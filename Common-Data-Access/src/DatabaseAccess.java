import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Supports Oracle and MariaDB Databases.
 */

public class DatabaseAccess {

    private String user;
    private String password;
    private String connectionUrl;

    public DatabaseAccess(String host, String port, String dbName, String user, String password, String dbVendor) throws DbVendorNotSupportedException {
        this.user = user;
        this.password = password;
        if("mariadb".equals(dbVendor)){
            initMariaDb(host,port,dbName);
        }else if("oracle".equals(dbVendor)){
            initOracleDb(host, port, dbName);
        }
        throw new DbVendorNotSupportedException(dbVendor+" is not supported. User oracle or mariadb");
        //throw new RuntimeException(dbVendor + " is currently not supported. Use oracle or mariadb!");
    }
    private void initMariaDb(String host, String port, String dbName){
        this.connectionUrl = "jdbc:mariadb://" + host + ":" + port + "/" + dbName;
        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver",e);
        }
    }

    private void initOracleDb(String host, String port, String dbName){
        this.connectionUrl = "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver",e);
        }
    }

    public Connection connection(){
        System.out.println("Connection to database");
        try {
            return DriverManager.getConnection(this.connectionUrl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    public void close(Connection connection){
        System.out.println("Closing connection");
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection: " + throwables.getMessage());
        }
    }
}




