public class DbVendorNotSupportedException extends Exception{

    public DbVendorNotSupportedException(String message){
        super(message);
    }
}
