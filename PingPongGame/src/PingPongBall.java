import java.awt.*;

public class PingPongBall extends Thread {

    private int xPos;
    private int yPos;
    private PingPongPanel canvas;
    private final PingPongBar bar;
    private int sleepTime = 50;

    private int xDirection = 1;
    private int yDirection = 1;

    private boolean move = true;

    private final static int DELTA = 2;

    public PingPongBall(PingPongPanel canvas, int xPos, int yPos, PingPongBar bar) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.canvas = canvas;
        this.bar = bar;
    }

    @Override
    public void run() {
        show();
        while (move) {
            move();
            show();
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void move() {
        if (xPos >= canvas.getWidth() - 20) {
            xDirection = -1;
        } else if (xPos <= 0) {
            xDirection = 1;
        }
        if (yPos + 20 >= canvas.getHeight() - 20) {
            if (this.bar.ballOnBar(this.xPos)) {
                yDirection = -1;
                if(this.sleepTime >10){
                    this.sleepTime -= 10;
                }

            }
        } else if (yPos <= 0) {
            yDirection = 1;
        }

        if(yPos > canvas.getHeight()){
            stopMoving();
        }
        xPos += DELTA * xDirection;
        yPos += DELTA * yDirection;
    }

    private void stopMoving() {
        move = false;

    }

    private void show() {
        Graphics g = canvas.getGraphics();
        g.setColor(Color.YELLOW);
        g.fillOval(xPos - DELTA * xDirection, yPos - DELTA * yDirection, 20, 20);
        g.setColor(Color.RED);
        g.fillOval(this.xPos, this.yPos, 20, 20);

    }
}


