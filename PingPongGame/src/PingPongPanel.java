import javax.swing.*;
import java.awt.*;



public class PingPongPanel extends JPanel {

    private PingPongBar bar;

    public PingPongPanel(){

        setBackground(Color.YELLOW);
        setFocusable(true);
        }



        public void initBar(){
            bar = new PingPongBar(this);
            addKeyListener(new BarMovingKeyAdapter(bar));
            addMouseListener(new BallCreatingMouseAdapter(this,bar));
        }

    public PingPongBar getBar() {
        return bar;
    }
}

