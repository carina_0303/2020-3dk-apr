import java.awt.*;


public class PingPongBar {

    private int xPos;
    private final PingPongPanel canvas;
    private static final int WIDTH = 60;


    public PingPongBar(PingPongPanel canvas) {
        this.canvas = canvas;
        this.xPos = 10;
        try {
            Thread.sleep(100);
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
        show();
    }

    private void show(){
        Graphics g = this.canvas.getGraphics();
        g.setColor(Color.BLUE);
        g.fillRect(xPos,this.canvas.getHeight()-20,WIDTH,5);
    }

    private void erase(){
        Graphics g = this.canvas.getGraphics();
        g.setColor(Color.YELLOW);
        g.fillRect(xPos,this.canvas.getHeight()-20,WIDTH,5);
    }

    public void moveRight() {
        if(xPos + WIDTH < this.canvas.getWidth()){
            erase();
            this.xPos += 5;
            show();
        }

    }

    public void moveLeft() {
        if(xPos > 0){
            erase();
            this.xPos -= 5;
            show();
        }
    }

    public boolean ballOnBar(int ballXPos){
        return ballXPos>= this.xPos && ballXPos <= this.xPos+WIDTH;
    }
}
