public class PingPongMain {
    public static void main(String[] args) {

        System.out.println("PING_PONG_BALLS: "+System.getenv("PING_PONG_BALLS"));



        /*if(args.length == 0){
            System.err.println("Parameter 'numberOfBalls' missing");
            System.exit(-1);
        }
        String numberOfBalls = args[0];*/

        String numberOfBalls = System.getenv("PING_PONG_BALLS");

        if(numberOfBalls == null){
            System.err.println("Variable PING_PONG_BALLS doesn't exist. Please set it with a valid number!");
            System.exit(-1);

        }

        int n= 0;
        try{
            n= Integer.parseInt(numberOfBalls);
        }catch (NumberFormatException e){
            System.err.println("Paramenter 'numberOfBalls' must be a number");
            System.exit(-1);
        }


        new PingPongGui(n);
    }
}
