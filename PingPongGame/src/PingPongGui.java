import javax.swing.*;


public class PingPongGui extends JFrame {

    public PingPongGui(int numberOfBalls) {
        super("PingPong");

        PingPongPanel canvas = new PingPongPanel();
        setContentPane(canvas);

        setSize(500,400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        canvas.initBar();



        for(int i = 0; i< numberOfBalls; i++){
            PingPongBall ball = new PingPongBall(canvas, i*20+10,i*20+10, canvas.getBar());
            ball.start();
        }
    }
}
