public class PersonThread extends Thread {

    private Account account;

    public PersonThread(Account account,String name) {
        super(name);
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            account.withdrawMoney(10,getName());
            if (account.getBalance()<0){
                System.out.println("account is overdrawn (balance: " + account.getBalance()+ ")");
            }
        }
    }


}
