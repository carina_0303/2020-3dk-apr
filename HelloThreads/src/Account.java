public class Account {

    private int balance;

    public Account(int balance) {
        this.balance = balance;
    }

    public void withdraw(int amount){
        balance -= amount;
    }

    public int getBalance() {
        return balance;
    }

    public synchronized void withdrawMoney(int amount, String name) {
            if (getBalance() >= amount) {
                System.out.println(name + " is going to withdraw");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                withdraw(amount);
                System.out.println(name + " completes the withdrawal");
            } else {
                System.out.println("Not enough in account for " + name + " " + getBalance());
            }
        }

}

