import javax.swing.*;
import java.awt.*;

public class DrawingProgramm extends JFrame {

    public DrawingProgramm(String title){
        super(title);

        Canvas canvas = new Canvas();
        add(canvas);

        setVisible(true);
        setSize(200,200);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}
