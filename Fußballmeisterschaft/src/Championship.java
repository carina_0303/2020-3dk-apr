import java.util.ArrayList;
import java.util.List;

public class Championship {
    private int id;
    private League league;
    private List<Team> teams;

    public int getId() {
        return id;
    }

    public League getLeague() {
        return league;
    }

    public List<Team> getTeam() {
        return teams;
    }

    public Championship(int id, League league){
        this.id = id;
        this.league = league;
        this.teams = new ArrayList<>();

    }
    public void addTeam (Team t){
        this.teams.add(t);
    }

    @Override
    public String toString() {
        return "Championship{" +
                "id=" + id +
                ", league=" + league +
                ", teams=" + teams +
                '}';
    }
}
