public class League {
    private int id;
    private String name;
    private String date;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public League(int id, String name, String date){
        this.id= id;
        this.name= name;
        this.date = date;

    }

    @Override
    public String toString() {
        return "League{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}

