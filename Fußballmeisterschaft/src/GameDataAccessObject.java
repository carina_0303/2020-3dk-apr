import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GameDataAccessObject {

    private final String user;

    private final String password;

    private final String databaseUrl;

    public GameDataAccessObject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.databaseUrl = "jdbc:mariadb://" + host + ":" + port + "/" + database;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not lad database driver", e);
        }
    }
    public void insert(List<Game> games) {
        Connection connection = connect();
        String sql = "insert into game (home_team, away_team, score_home, score_away, game_date) VALUES (?, ?, null, null, ?)";
        try {
            for (Game g : games) {
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, g.getHomeTeam().getId());
                preparedStatement.setInt(2, g.getAwayTeam().getId());
                preparedStatement.setDate(3, g.getPlaydate());
                preparedStatement.execute();
                //System.out.println("Game successfully inserted");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            ChampionshipDataAccessObject.close(connection);
        }
    }public Connection connect() {
        try {
            return DriverManager.getConnection(this.databaseUrl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    public static void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}

