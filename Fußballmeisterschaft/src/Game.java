import java.sql.Date;
public class Game {
    private Team homeTeam;
    private Team awayTeam;
    private int scoreHome;
    private int scoreAway;
    private Date playdate;
    public Game(Team homeTeam, Team awayTeam, Date playdate) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.playdate = playdate;
    }
    public Team getHomeTeam() {
        return homeTeam;
    }
    public Team getAwayTeam() {
        return awayTeam;
    }
    public Date getPlaydate() {
        return playdate;
    }
}