import java.sql.*;
import java.util.ArrayList;

public class ChampionshipDataAccessObject {


    private final String user;

    private final String password;

    private final String databaseUrl;

    public ChampionshipDataAccessObject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.databaseUrl = "jdbc:mariadb://" + host + ":" + port + "/" + database;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not lad database driver", e);
        }
    }
    public Championship loadForLeague(String name){
        Connection connection = connect();
        Statement statement = null;
        String sql = "select c.id, l.id, l.name, l.year, t.id, t.name\n" +
                "from championship c\n" +
                "join league l on c.league = l.id\n" +
                "join team t on c.team = t.id\n" +
                "where l.name = '" + name + "';";
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            Championship championship = null;
            if (resultSet.next()) {
                League league = new League(resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4));
                championship = new Championship(resultSet.getInt(1), league);
                championship.addTeam(new Team(resultSet.getInt(5), resultSet.getString(6)));
            }
            while (resultSet.next()) {
                championship.addTeam(new Team(resultSet.getInt(5), resultSet.getString(6)));
            }
            return championship;
        } catch (SQLException throwables) {
            System.err.println("Could not create list");
            throwables.printStackTrace();
        } finally {
            close(connection);
        }
        return null;

    }
    public ArrayList<Team> extractTeams(ResultSet resultSet) throws SQLException {
        ArrayList<Team> teamList = new ArrayList<>();
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String name = resultSet.getString(2);
            teamList.add(new Team(id,name));
        }
        return teamList;
    }
    public ArrayList readAllTeams() {
        Connection connection = connect();
        Statement statement = null;
        ArrayList<Team> teamlist = new ArrayList<>();
        try {
            statement = connection.createStatement();
            String sql = "select * from team";
            ResultSet resultSet = statement.executeQuery(sql);
            return (ArrayList) extractTeams(resultSet);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create teamlist", throwables);
        } finally {
            close(connection);
        }
    }
    public Connection connect() {
        try {
            return DriverManager.getConnection(this.databaseUrl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    public static void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
