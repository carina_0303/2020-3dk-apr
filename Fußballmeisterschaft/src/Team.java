public class Team {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Team(int id, String name){
        this.id = id;
        this.name = name;


    }

    @Override
    public String toString() {
        return id +" " + name;
    }
}
