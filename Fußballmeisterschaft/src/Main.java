import java.awt.*;

import java.sql.Array;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws SQLException {
        /*Team heidenreichstein = new Team (1,"Heidenreichstein");
        Team gastern = new Team (2, "Gastern");
        Team vitis = new Team (3, "Vitis");

        League waldviertel = new League(1,"1. Klasse Waldviertel","2021/2022");

        //Championship meisterschaft = new Championship(1, waldviertel);
        //meisterschaft.addTeam(heidenreichstein);
        //meisterschaft.addTeam(gastern);
        //meisterschaft.addTeam(vitis);*/


        ChampionshipDataAccessObject championshipDao = new ChampionshipDataAccessObject("localhost", "3306", "fussballmannschaft", "root", "");
        Championship meisterschaft = championshipDao.loadForLeague("1. Klasse Waldviertel");

        //print(meisterschaft);
        //printGameSchedule(championshipDao.readAllTeams());


        ArrayList games = new ArrayList();
        //games = createGame(meisterschaft);



        GameDataAccessObject gameDao = new GameDataAccessObject("localhost","3306","fussballmannschaft","root","");
        Game g1 = new Game(meisterschaft.getTeam().get(0),meisterschaft.getTeam().get(1),Date.valueOf(LocalDate.of(2021,6,17)));
        Game g2 = new Game(meisterschaft.getTeam().get(3),meisterschaft.getTeam().get(4),Date.valueOf(LocalDate.of(2021,6,17)));
        //gameDao.insert(games);


        ArrayList<Team> teams = championshipDao.readAllTeams();
        //teams.add("1");
        //teams.add("2");
        //teams.add("3");
        //teams.add("4");
        //teams.add("5");
        //teams.add("6");
        // ziel is spieltage mit namen der Team ogebn


        Team joker = teams.remove(teams.size()-1);

        ArrayList<Game> game = new ArrayList<>();

        for(int i = 0;i< teams.size();i++){
            System.out.println((i+1)+". Spieltag");
            System.out.println("-----------------");
            int j = 0;
            while(j< teams.size()/2){
                System.out.println(teams.get(j)+ " : " + teams.get(teams.size()-1-j));
                game.add(new Game(teams.get(j),teams.get(teams.size()-1-j),Date.valueOf(LocalDate.of(2021,6,23))));
                j++;
            }
            System.out.println(teams.get(j) + ":" + joker);

            Team t = teams.remove(0);
            teams.add(t);
            gameDao.insert(game);

        }

    }
    /*public static ArrayList<Game> createGame(Championship championship){
        ArrayList<Game> games = new ArrayList<>();
        games.add(new Game(4,championship.getTeam().get(1).getId(), championship.getTeam().get(2).getId(),Date.valueOf(LocalDate.of(2021,6,17))));
        games.add(new Game(5,championship.getTeam().get(3).getId(), championship.getTeam().get(4).getId(),Date.valueOf(LocalDate.of(2021,5,04))));
        games.add(new Game(6,championship.getTeam().get(5).getId(), championship.getTeam().get(6).getId(),Date.valueOf(LocalDate.of(2021,7,12))));
     return games;
    }*/

    public static void printGameSchedule(ArrayList teamList) {
        ArrayList teamlist2 = new ArrayList(); //Zweite Arrayliste zum Ändern
        for (int i = 0; i < teamList.size() - 1; i++) {
            System.out.println("Spieltag " + (i + 1) + ": "); //Liste der Spieltage erzeugen
            if(i != 0){ //wenn es nicht der erste Spieltag ist, dann soll die Liste geändert werden
                for (int k = 0; k <= teamList.size()-1; k++) {
                    if(k>=0 && k<=teamList.size()-3){ // Alle Teams, die nicht das Erste oder das Letzte sind werden um eine Position nach vorne gereiht
                        teamlist2.add(teamList.get(k+1));
                    }else if(k == teamList.size()-2){//Das erste Team wird an die vorletzte Stelle gereiht.
                        teamlist2.add(teamList.get(0));
                    }else{
                        teamlist2.add(teamList.get(teamList.size()-1));//Der Joker beibt an der gleichen Stelle
                    }
                }
                teamList.clear();
                for (int k = 0; k < teamlist2.size(); k++) {
                    teamList.add(teamlist2.get(k));
                }
                teamlist2.clear();
            }
            for (int j = 0; j < teamList.size() / 2; j++) {
                int joker = teamList.size();
                if (j == teamList.size() / 2-1) {//Das Letzte Spiel wird ausgelost
                    System.out.println("Team " + (teamList.get(j)) + ":" + "Team " + joker);


                } else { //alle anderen Spiele werden ausgelost
                    System.out.println("Team " + (teamList.get(j)) + ":" + "Team " + (teamList.get(joker - j -2)));

                }
                }
            }
        }
        public static void print (Championship championship){
            String headline = championship.getLeague().getName() + "(" + championship.getLeague().getDate() + ")";
            System.out.println(headline);
            int len = headline.length();
            for (int i = 0; i < len; i++) {
                System.out.print("-");
            }
            System.out.println();


            for (Team t : championship.getTeam()) {
                System.out.println(" " + t.getName());
            }

        }

    }
