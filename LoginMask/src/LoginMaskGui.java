import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class LoginMaskGui extends JFrame {

    public LoginMaskGui(String title) {
        super(title);

        UserDataAccessObject userDao = new UserDataAccessObject("localhost","3306","login_gui_db","root","");

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(null);
        JLabel userLabel = new JLabel("User");
        userLabel.setBounds(10,10,128,20);
        mainPanel.add(userLabel);
        JTextField userField = new JTextField();
        userField.setBounds(150,10,150,20);
        mainPanel.add(userField);

        JLabel pwLabel = new JLabel("Passwort");
        pwLabel.setBounds(10,50,120,20);
        mainPanel.add(pwLabel);

        JPasswordField pwField = new JPasswordField();
        pwField.setBounds(150,50,150,20);
        mainPanel.add(pwField);

        JButton loginButton = new JButton("Login");
        loginButton.setBounds(10,80,100,30);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("login: "+userDao.login(userField.getText(), new String (pwField.getPassword())));
                System.out.println("securelogin: "+userDao.secureLogin(userField.getText(), new String (pwField.getPassword())));
            }
        });
        mainPanel.add(loginButton);

        mainPanel.setPreferredSize(new Dimension(500,200));
        add(mainPanel,BorderLayout.CENTER);

        JPanel southPanel = new JPanel();
        southPanel.add(new JButton("Registrieren"));
        southPanel.add(new JButton("Passwort vergessen"));
        add(southPanel, BorderLayout.SOUTH);

        setSize(600,400);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}

