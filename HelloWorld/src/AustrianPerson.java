public class AustrianPerson extends Person {

    public AustrianPerson(String name) {
        super(name,"österreichisch");
        if(name.equals("Johann")){
            this.name = "Hauns";
        }
    }

    @Override
    public String greet() {
        return "Servas " + name;
    }
}
