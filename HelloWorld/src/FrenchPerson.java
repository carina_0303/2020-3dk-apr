public class FrenchPerson extends Person {
    public FrenchPerson(String name) {
        super(name,"franzöisch");
    }

    @Override
    public String greet() {
        return "Bonjour " + name;
    }
}
