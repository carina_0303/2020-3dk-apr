public class SpanishPerson extends Person{

    public SpanishPerson(String name) {
            super(name,"spanisch");

        }

    public String greet(){
        return "Hola "+ name;
    }
}
