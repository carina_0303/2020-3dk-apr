public abstract class Person {

    protected String name;

    private String motherTongue;

    public Person(String name, String motherTongue) {
            if (name.equals("Johann")){
                this.name = "Hansi";
            }else{
                this.name = name;
            }
            this.motherTongue = motherTongue;
        }

    public abstract String greet();

    public String getMotherTongue() {
        return motherTongue;
    }
}
