public class EnglishPerson extends Person {

    public EnglishPerson(String name) {
        super(name,"englisch");
    }

    public String greet(){
        return "Hello " + name;
    }

}
