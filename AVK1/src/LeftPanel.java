import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class LeftPanel extends JPanel {
    private LayoutmanagerGui layoutmanagerGui; // wozu ?
    private static JScrollPane scrollPane; // wozu ?


    public LeftPanel(LayoutmanagerGui layoutmanagerGui){ // wozu der Parameter ?
        setPreferredSize(new Dimension(450,250));
        this.layoutmanagerGui = layoutmanagerGui;
        setLayout(new BorderLayout()); // sollte das null-Layout sein ?!? -1
                                       // hier wäre noch ein Layout-Manager sinnvoll: -1


        JLabel firstname = new JLabel("Vorname: ");
        firstname.setBounds(10,20,120,20);

        JTextField fistnameTextField = new JTextField();
        fistnameTextField.setBounds(130,20,150,20);

        JLabel secondname = new JLabel("Nachname:");
        secondname.setBounds(10,50,120,20);

        JTextField secondnameTextField = new JTextField();
        secondnameTextField.setBounds(130,50,150,20);

        JLabel street = new JLabel("Straße:");
        street.setBounds(10,80,120,20);

        JTextField streetTextField = new JTextField();
        streetTextField.setBounds(130,80,150,20);

        JLabel plz = new JLabel("PLZ:");
        plz.setBounds(10,110,120,20);

        JTextField plzTextField = new JTextField();
        plzTextField.setBounds(130,110,50,20);

        JLabel place = new JLabel("Ort: ");
        place.setBounds(10,140,120,20);

        JTextField placeTextField = new JTextField();
        placeTextField.setBounds(130,140,150,20);



        add(placeTextField);
        add(place);
        add(plzTextField);
        add(firstname);
        add(fistnameTextField);
        add(secondname);
        add(secondnameTextField);
        add(street);
        add(streetTextField);
        add(plz);



}

}


