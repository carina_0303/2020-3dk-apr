import javax.swing.*;
import java.awt.*;

public class LayoutmanagerGui extends JFrame{ // Klassenname ist nicht gut - GuestRegistrationGui oder dergleichen wäre besser

    public LayoutmanagerGui(String title){
        super(title);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setSize(600,700);

        JPanel titlePanel = new JPanel();
        contentPane.add(titlePanel,BorderLayout.NORTH);

        JLabel titleLabel = new JLabel("Gästeregistrierung");
        titleLabel.setFont(new Font("Arial", Font.PLAIN, 30));
        titlePanel.add(titleLabel);

        JPanel buttonPanel = new JPanel();
        contentPane.add(buttonPanel,BorderLayout.SOUTH);

        JButton saveButton = new JButton("Speichern");
        buttonPanel.add(saveButton);

        JButton backButton = new JButton("Zurücksetzen");
        buttonPanel.add(backButton);

        LeftPanel leftPanel = new LeftPanel(this);
        RightPanel rightPanel = new RightPanel(this);

        setContentPane(contentPane);
        contentPane.add(leftPanel,BorderLayout.WEST);
        contentPane.add(rightPanel,BorderLayout.EAST);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,600);
        setVisible(true);

        pack();
}


}
