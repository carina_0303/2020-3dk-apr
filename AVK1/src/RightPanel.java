import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class RightPanel extends JPanel {
    private LayoutmanagerGui layoutmanagerGui;


    public RightPanel(LayoutmanagerGui layoutmanagerGui){
        setPreferredSize(new Dimension(450,250));
        this.layoutmanagerGui = layoutmanagerGui;
        setLayout(new BorderLayout()); // sollte das null-Layout sein ?!?


        JLabel telefone = new JLabel("Telefonnummer: ");
        telefone.setBounds(10,20,120,20);

        JTextField telefoneTextField = new JTextField();
        telefoneTextField.setBounds(130,20,150,20);

        JLabel email = new JLabel("E-Mail:");
        email.setBounds(10,50,120,20);

        JTextField emailTextField = new JTextField();
        emailTextField.setBounds(130,50,150,20);

        JLabel tableNumber = new JLabel("Tisch.Nr:");
        tableNumber.setBounds(10,80,120,20);

        JTextField tableNumberTextField = new JTextField();
        tableNumberTextField.setBounds(130,80,150,20);



        add(telefone);
        add(telefoneTextField);
        add(email);
        add(emailTextField);
        add(tableNumberTextField);
        add(tableNumber);

    }

}

