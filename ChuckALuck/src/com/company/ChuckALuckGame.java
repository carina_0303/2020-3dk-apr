package com.company;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ChuckALuckGame {

    private AccountPartGui accountPartGui;
    private SetNumberPartGui setNumberPartGui;
    private ThrowDicePartGui throwDicePartGui;
    private int currentAccount;
    private int accordanceNumber;
    ArrayList<String> numbers = new ArrayList<String>();

    public ChuckALuckGame(AccountPartGui accountPartGui, SetNumberPartGui setNumberPartGui, ThrowDicePartGui throwDicePartGui) {
        this.accountPartGui = accountPartGui;
        this.setNumberPartGui = setNumberPartGui;
        this.throwDicePartGui = throwDicePartGui;
    }

    public void EnableButton() { // Methoden-Name klein
        //While account is 0 the throw dice button is enabled
        if(accountPartGui.getLblScore().getText().equals("0")) {
            throwDicePartGui.getBtnDice().setEnabled(false);
            accountPartGui.getBtnAddStake().setEnabled(true);
        }
    }

    public void FillAccount(){
        accountPartGui.getBtnAddStake().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Stock Account when the button is pushed
                throwDicePartGui.getBtnDice().setEnabled(true);
                currentAccount = Integer.parseInt(accountPartGui.getLblScore().getText());
                currentAccount += 5;
                accountPartGui.getLblScore().setText(String.valueOf(currentAccount));

            }
        });
    }

    public void RandomNumbersAndCheckAccordanceAndUpdateAccount(){
        throwDicePartGui.getBtnDice().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                accountPartGui.getBtnAddStake().setEnabled(false);
                //Random Number
                throwDicePartGui.getLblDice1().setText(String.valueOf((int) (Math.random() * 6) + 1));
                throwDicePartGui.getLblDice2().setText(String.valueOf((int) (Math.random() * 6) + 1));
                throwDicePartGui.getLblDice3().setText(String.valueOf((int) (Math.random() * 6) + 1));

                //Fill in ArrayList
                numbers.add(throwDicePartGui.getLblDice1().getText());
                numbers.add(throwDicePartGui.getLblDice2().getText());
                numbers.add(throwDicePartGui.getLblDice3().getText());

                //Check Accordance
                for (int i = 0; i < 3; i++) {
                    if (setNumberPartGui.getTxtSetNumber().getText().equals(numbers.get(i))){
                        accordanceNumber++;
                    }
                }
                //Update account
                // du könntest in den folgenden Zeilen gleich die accordanceNumber Variable nehmen und der currentAccount
                // Variable zuweisen, damit würdest du dir einige Zeilen Code sparen
                if(accordanceNumber == 3){
                    currentAccount = Integer.parseInt(accountPartGui.getLblScore().getText());
                    currentAccount += 3;
                    accountPartGui.getLblScore().setText(String.valueOf(currentAccount));
                }else if(accordanceNumber == 2){
                    currentAccount = Integer.parseInt(accountPartGui.getLblScore().getText());
                    currentAccount += 2;
                    accountPartGui.getLblScore().setText(String.valueOf(currentAccount));
                }else if(accordanceNumber == 1){
                    currentAccount = Integer.parseInt(accountPartGui.getLblScore().getText());
                    currentAccount += 1;
                    accountPartGui.getLblScore().setText(String.valueOf(currentAccount));
                }else if (accordanceNumber == 0){
                    currentAccount = Integer.parseInt(accountPartGui.getLblScore().getText());
                    currentAccount -= 1;
                    accountPartGui.getLblScore().setText(String.valueOf(currentAccount));
                }


                //set Array and accordanceNumber to 0
                numbers.clear();
                accordanceNumber = 0;

                //Enables Button when account is 0
                EnableButton();






        }

        });
    }



}



