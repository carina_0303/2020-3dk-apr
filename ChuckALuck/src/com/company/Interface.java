package com.company;

import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame
{
    private final String[] ARR = new String[]{"lblDice1", "lblDice2","lblDice3"};

    private JPanel heading = new JPanel();
    private JLabel lblHeading = new JLabel("CHUCK A LUCK");


    private AccountPartGui myAccount = new AccountPartGui();
    private SetNumberPartGui myNumber = new SetNumberPartGui();
    private ThrowDicePartGui myDice = new ThrowDicePartGui();
    private ChuckALuckGame chuckALuckGame = new ChuckALuckGame(myAccount,myNumber,myDice);



    public Interface(String title)
    {
        super(title);

        chuckALuckGame.EnableButton();
        chuckALuckGame.RandomNumbersAndCheckAccordanceAndUpdateAccount();
        chuckALuckGame.FillAccount();



        add(myAccount, BorderLayout.WEST);
        add(myNumber, BorderLayout.CENTER);
        add(myDice, BorderLayout.EAST);

        heading.setBackground(Color.BLUE);

        heading.add(lblHeading);
        lblHeading.setForeground(Color.WHITE);
        lblHeading.setFont(new Font("Times New Roman", Font.BOLD, 18));
        lblHeading.setHorizontalAlignment(SwingConstants.CENTER);
        add(heading, BorderLayout.NORTH);
    }
}