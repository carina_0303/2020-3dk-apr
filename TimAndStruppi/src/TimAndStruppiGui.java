import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimAndStruppiGui extends JFrame {

    public TimAndStruppiGui(String title){
        super(title);
        setLayout(new FlowLayout());

        JButton buttonTim = new JButton("Tim");
        buttonTim.addActionListener(new ConsoleOutputActionListener() );
        add(buttonTim);

        JButton buttonAnd = new JButton("and");
        buttonAnd.addActionListener(new ConsoleOutputActionListener());
        add(buttonAnd);

        JButton buttonStruppi = new JButton("Struppi");
        buttonStruppi.addActionListener(new ConsoleOutputActionListener());
        add(buttonStruppi);

        addWindowListener(new WindowClosingListener());

        setVisible(true);
        setSize(200,200);

    }
}
