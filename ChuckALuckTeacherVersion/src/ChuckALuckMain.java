import javax.swing.*;

public class ChuckALuckMain
{
    public static void main(String[] args)
    {
        ChuckALuckGui myChuckALuckGui = new ChuckALuckGui("chuck a luck");
        myChuckALuckGui.setSize(480,250);
        myChuckALuckGui.setLocationRelativeTo(null);
        myChuckALuckGui.setResizable(false);
        myChuckALuckGui.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        myChuckALuckGui.setVisible(true);
    }
}
