import javax.swing.*;
import java.awt.*;

public class ChuckALuckGui extends JFrame {

    private JPanel heading = new JPanel();
    private JLabel lblHeading = new JLabel("CHUCK A LUCK");

    private AccountPanel myAccountPanel;
    private SetNumberPanel numberPanel;
    private Game game;
    private ThrowDicePanel myDice;

    public ChuckALuckGui(String title) {
        super(title);

        game = new Game(5);
        myDice = new ThrowDicePanel(game);
        numberPanel = new SetNumberPanel(game);
        myAccountPanel = new AccountPanel(game,myDice, numberPanel);

        add(myAccountPanel, BorderLayout.WEST);
        add(numberPanel, BorderLayout.CENTER);
        add(myDice, BorderLayout.EAST);

        heading.setBackground(Color.BLUE);

        heading.add(lblHeading);
        lblHeading.setForeground(Color.WHITE);
        lblHeading.setFont(new Font("Times New Roman", Font.BOLD, 18));
        lblHeading.setHorizontalAlignment(SwingConstants.CENTER);
        add(heading, BorderLayout.NORTH);
    }
}