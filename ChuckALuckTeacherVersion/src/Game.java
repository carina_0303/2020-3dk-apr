import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {

    private int[] diceValues;

    private int value;

    private int currentAccountValue;

    private Random random;

    private List<ScoreChangeListener> scoreChangeListeners;

    private int insertion;

    public Game(int insertion) {
        this.insertion = insertion;
        random = new Random();
        diceValues = new int[3];
        currentAccountValue = 0;
        this.currentAccountValue = insertion;
        scoreChangeListeners = new ArrayList<>();
    }

    public void throwDices() {
        for (int i = 0; i < 3; i++) {
            diceValues[i] = nextDiceValue();
        }
    }

    public void calculate() {
        int noOfWins = 0;
        for (int v : diceValues) {
            if ( this.value == v ) {
                noOfWins++;
            }
        }
        if ( noOfWins > 0 ) {
            currentAccountValue += noOfWins;
        } else {
            currentAccountValue -= 1;
        }
        notifyScoreChangeListeners(currentAccountValue);
    }

    private void notifyScoreChangeListeners(int newScore) {
        for (ScoreChangeListener listener : scoreChangeListeners) {
            listener.scoreChanged(newScore);
        }
    }

    private int nextDiceValue() {
        return random.nextInt(6) + 1;
    }

    public int[] getDiceValues() {
        return diceValues;
    }

    public int getCurrentAccountValue() {
        return currentAccountValue;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void registerScoreChangeListener(ScoreChangeListener listener) {
        scoreChangeListeners.add(listener);
    }

    public int getInsertion() {
        return insertion;
    }
}
