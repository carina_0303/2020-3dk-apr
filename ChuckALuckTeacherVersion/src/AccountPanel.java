import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AccountPanel extends JPanel implements ScoreChangeListener {

    private JLabel lblScore;
    private JButton btnAddStake;
    private ThrowDicePanel throwDicePanel;
    private SetNumberPanel numberPanel;
    private Game game;

    public AccountPanel(Game game, ThrowDicePanel throwDicePanel, SetNumberPanel numberPanel) {
        this.throwDicePanel = throwDicePanel;
        this.numberPanel = numberPanel;
        this.game = game;

        setLayout(new FlowLayout(FlowLayout.CENTER,10,20));
        setBorder(new EmptyBorder(6,10,10,10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.LIGHT_GRAY);

        JLabel lblAccount = new JLabel("Konto");
        lblAccount.setHorizontalAlignment(SwingConstants.CENTER);
        lblAccount.setPreferredSize(new Dimension(120,30));
        lblAccount.setOpaque(true);
        lblAccount.setBackground(Color.WHITE);
        add(lblAccount);

        lblScore = new JLabel();
        lblScore.setText("0");
        lblScore.setHorizontalAlignment(SwingConstants.CENTER);
        lblScore.setPreferredSize(new Dimension(30,30));
        lblScore.setOpaque(true);
        lblScore.setBackground(Color.WHITE);
        add(lblScore);

        btnAddStake = new JButton("Einsatz zahlen");
        btnAddStake.setPreferredSize(new Dimension(120,30));
        add(btnAddStake);

        btnAddStake.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lblScore.setText(String.valueOf(game.getInsertion()));
                throwDicePanel.getButtonDice().setEnabled(true);
                numberPanel.getNumberTextField().requestFocus();
                btnAddStake.setEnabled(false);
            }
        });

        game.registerScoreChangeListener(this);
    }

    @Override
    public void scoreChanged(int newScore) {
        this.lblScore.setText(String.valueOf(newScore));
        if ( newScore == 0 ) {
            this.btnAddStake.setEnabled(true);
            this.throwDicePanel.reset();
            this.numberPanel.getNumberTextField().setText("");
            lblScore.setText(String.valueOf(this.game.getInsertion()));
        }
    }
}

