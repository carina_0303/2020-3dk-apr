import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ThrowDicePanel extends JPanel {
    private JButton btnDice;
    private JLabel lblDice1;
    private JLabel lblDice2;
    private JLabel lblDice3;

    private Game game;

    public ThrowDicePanel(Game game) {
        this.game = game;
        setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

        setBorder(new EmptyBorder(16, 10, 15, 10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.LIGHT_GRAY);

        JLabel lblDice = new JLabel("Würfeln");
        lblDice.setPreferredSize(new Dimension(120, 30));
        lblDice.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice.setOpaque(true);
        lblDice.setBackground(Color.WHITE);
        add(lblDice);

        JPanel dicePanel = new JPanel();
        dicePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        dicePanel.setOpaque(false);
        lblDice1 = new JLabel("");
        lblDice1.setPreferredSize(new Dimension(30, 30));
        lblDice1.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice1.setOpaque(true);
        lblDice1.setBackground(Color.WHITE);
        dicePanel.add(lblDice1);

        lblDice2 = new JLabel("");
        lblDice2.setPreferredSize(new Dimension(30, 30));
        lblDice2.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice2.setOpaque(true);
        lblDice2.setBackground(Color.WHITE);
        dicePanel.add(lblDice2);

        lblDice3 = new JLabel("");
        lblDice3.setPreferredSize(new Dimension(30, 30));
        lblDice3.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice3.setOpaque(true);
        lblDice3.setBackground(Color.WHITE);
        dicePanel.add(lblDice3);

        add(dicePanel);

        btnDice = new JButton("Würfeln");
        btnDice.setPreferredSize(new Dimension(120, 30));
        btnDice.setEnabled(false);
        add(btnDice);

        btnDice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.throwDices();
                lblDice1.setText(String.valueOf(game.getDiceValues()[0]));
                lblDice2.setText(String.valueOf(game.getDiceValues()[1]));
                lblDice3.setText(String.valueOf(game.getDiceValues()[2]));
                game.calculate();
            }
        });
    }


    public JButton getButtonDice() {
        return btnDice;
    }

    public void reset() {
        this.lblDice1.setText("");
        this.lblDice2.setText("");
        this.lblDice3.setText("");
        this.btnDice.setEnabled(false);
    }
}

