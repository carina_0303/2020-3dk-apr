/**
 * Implements the Observer Pattern
 *
 * Classes should implement this interface if they want to be notified when the score changes
 */
public interface ScoreChangeListener {

    void scoreChanged(int newScore);

}
