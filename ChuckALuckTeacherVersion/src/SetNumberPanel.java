import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class SetNumberPanel extends JPanel {

    private JTextField numberTextField;

    public SetNumberPanel(Game game) {
        setLayout(new FlowLayout(FlowLayout.CENTER,10,20));
        setBorder(new EmptyBorder(6,10,10,10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.MAGENTA);

        JLabel lblSetNumber = new JLabel("Zahl setzen");
        lblSetNumber.setHorizontalAlignment(SwingConstants.CENTER);
        lblSetNumber.setPreferredSize(new Dimension(120,30));
        lblSetNumber.setOpaque(true);
        lblSetNumber.setBackground(Color.WHITE);
        add(lblSetNumber);

        numberTextField = new JTextField();
        numberTextField.setHorizontalAlignment(SwingConstants.CENTER);
        numberTextField.setPreferredSize(new Dimension(30,30));
        numberTextField.setOpaque(true);
        numberTextField.setBackground(Color.WHITE);

        numberTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
               if ( ! numberTextField.getText().isEmpty() ) {
                   game.setValue(Integer.valueOf(numberTextField.getText()));
               }
            }
        });
        add(numberTextField);

    }

    public JTextField getNumberTextField() {
        return numberTextField;
    }
}

