import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FunktionPlotter extends JFrame {
    private Canvas drawArea;
    private int function;

    public FunktionPlotter(String title) {
        super(title);
        setLayout(new FlowLayout());

        drawArea = new Canvas(this);

        add(drawArea);

        JPanel pnlButton = new JPanel();

        pnlButton.setLayout(new GridLayout(2, 1, 20, 20));
        JButton btnF1 = new JButton("tan(x)");
        btnF1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                function = 1;
                drawArea.repaint();
            }
        });

        JButton btnF2 = new JButton("x^2");
        btnF2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                function = 2;
                drawArea.repaint();
            }
        });
        pnlButton.add(btnF1);
        pnlButton.add(btnF2);

        add(pnlButton);
    }


        public int getFunction(){
            return function;
        }


    }

