import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class LowerWindow extends JPanel {

    private WindowManager windowManager;
    private static JScrollPane scrollPane;


    public LowerWindow(WindowManager windowManager){
        setPreferredSize(new Dimension(500,250));
        this.windowManager = windowManager;
        setLayout(new BorderLayout());


        scrollPane = new JScrollPane();
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(10,10,400,180);
        textArea.setBorder(new LineBorder(Color.GRAY));

        scrollPane.setBounds(10,10,480,180);
        scrollPane.getViewport().setBackground(Color.WHITE);
        scrollPane.getViewport().add(textArea,BorderLayout.CENTER);

        add(scrollPane,BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        add(buttonPanel,BorderLayout.SOUTH);

        JButton showArticleListButton = new JButton("Zeige Artikelliste");
        buttonPanel.add(showArticleListButton,BorderLayout.WEST);

        JButton closeButton = new JButton("Beenden");
        buttonPanel.add(closeButton,BorderLayout.EAST);

    }

}
