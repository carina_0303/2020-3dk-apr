import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class UpperWindow extends JPanel {

    private WindowManager windowManager;

    public UpperWindow(WindowManager windowManager){
        setPreferredSize(new Dimension(500,200));
        this.windowManager = windowManager;
        setLayout(null);

       JLabel article = new JLabel("Artikel");
       article.setBounds(10,10,70,20);
       article.setFont(new Font("Arial", Font.PLAIN, 20));

       JLabel articleNumber = new JLabel("Artikelnummer: ");
       articleNumber.setBounds(10,50,120,20);

       JLabel articleName = new JLabel("Artikelbezeichnung: ");
       articleName.setBounds(10,80,120,20);

       JLabel sellingPrice = new JLabel("Verkaufspreis: ");
       sellingPrice.setBounds(10,110,120,20);

       JLabel stock = new JLabel("Bestand: ");
       stock.setBounds(10,140,120,20);

       JTextField articleNumberTextField = new JTextField();
       articleNumberTextField.setBounds(150,50,200,20);

       JTextField articleNameTextField = new JTextField();
       articleNameTextField.setBounds(150,80,200,20);

       JTextField sellingPriceTextField = new JTextField();
       sellingPriceTextField.setBounds(150,110,200,20);

       JTextField stockTextField = new JTextField();
       stockTextField.setBounds(150,140,200,20);

       JButton newButton = new JButton("Neu");
       newButton.setBounds(390,10,100,20);

       JButton applyButton = new JButton("Anlegen");
       applyButton.setBounds(390,50,100,20);

       JButton changeButton = new JButton("Ändern");
       changeButton.setBounds(390,80,100,20);

       JButton clearButton = new JButton("Löschen");
       clearButton.setBounds(390,110,100,20);

       JButton searchButton = new JButton("Suchen");
       searchButton.setBounds(390,140,100,20);




       add(article);
       add(newButton);
       add(articleNumber);
       add(articleNumberTextField);
       add(applyButton);
       add(articleName);
       add(articleNameTextField);
       add(changeButton);
       add(sellingPrice);
       add(sellingPriceTextField);
       add(clearButton);
       add(stock);
       add(stockTextField);
       add(searchButton);
    }
}
