import javax.swing.*;
import java.awt.*;

public class WindowManager extends JFrame {


    public WindowManager(String title){
        super(title);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setSize(600,700);

        UpperWindow upperWindow = new UpperWindow(this);
        LowerWindow lowerWindow = new LowerWindow(this);

        setContentPane(contentPane);
        contentPane.add(upperWindow,BorderLayout.NORTH);
        contentPane.add(lowerWindow,BorderLayout.CENTER);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,600);
        setVisible(true);

        pack();
    }


}
