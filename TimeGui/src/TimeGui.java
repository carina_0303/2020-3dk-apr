import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeGui extends JFrame {

    private JLabel timeLabel;

    private Color[] colors = {Color.MAGENTA,Color.BLUE,Color.RED};
    private int currentColorPosition;
    private int time;



    public TimeGui (String title){
        super (title);
        setLayout(new FlowLayout());
        JPanel contentPane = new JPanel();
        setContentPane(contentPane);
        JTextField timeField = new JTextField(time);
        timeField.setPreferredSize(new Dimension(50,20));
        contentPane.add(timeField);


        JButton colourChange = new JButton("Farbwechsel starten");
        ColorSwitchingThread t = new ColorSwitchingThread(this);
        colourChange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Timer colorTimer = new Timer(1000*time, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    t.start();
                }
                });
                colorTimer.start();

            }
        });
        contentPane.add(colourChange);


        this.timeLabel = new JLabel(getTimeText());
        contentPane.add(timeLabel);

        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setTime();
            }
        });
        timer.start();
        pack();
    }

    public void setTime(){
        timeLabel.setText(getTimeText());

    }
    public String getTimeText(){
    DateTimeFormatter dateTimeFormatter= DateTimeFormatter.ofPattern("HH:mm:ss");
    return LocalTime.now().format(dateTimeFormatter);
}

    public void switchColor() {
        currentColorPosition++;
        if(currentColorPosition > colors.length-1){ currentColorPosition = 0;
        }
        timeLabel.setForeground(colors[currentColorPosition]);
    }
}
