import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MouseClickCounterGui extends JFrame {



    public MouseClickCounterGui(String title){
        super(title);
        setLayout(new FlowLayout());

        JButton button = new JButton("How many clicks?");
        add(button);

        JLabel label = new JLabel();
        add(label);

        MouseListener mouseclick = new MouseAdapter() {
            private int click;

            @Override
            public void mouseClicked(MouseEvent e) {
                this.click ++;
                button.addActionListener(e1 -> label.setText(String.valueOf(click)));
            }


        };
        addMouseListener(mouseclick);
        addWindowListener(new WindowClosingListener());
        setVisible(true);
        setSize(400,400);



    }
}
