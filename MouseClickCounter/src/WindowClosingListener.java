import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowClosingListener extends WindowAdapter {
    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("Window is closing now");;
        System.exit(0);
    }
}
