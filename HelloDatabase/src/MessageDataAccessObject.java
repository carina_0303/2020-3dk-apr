
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class MessageDataAccessObject {

    private DatabaseAccess databaseAccess;

    public MessageDataAccessObject(String host, String port, String database, String user, String password, String dbVendor) throws DbVendorNotSupportedException {

        try{
            databaseAccess = new DatabaseAccess(host, port,database,user,password,dbVendor);

        }catch (DbVendorNotSupportedException e){
            System.err.println("Could not start program!");
            System.exit(1);
        }


    }

    public List<Message> readAllMessages() {
        Connection connection = connect();
        Statement statement = null;
        try {
            String sql = "select * from message";
            statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            ArrayList<Message> messages = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String msg = resultSet.getString(2);
                String lng = resultSet.getString(3);
                messages.add(new Message(id, msg, lng));
            }
            close(connection);
            return messages;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }
    public Message read (int id){
        Message message = null;
        Connection connection = connect();
        Statement statement = null;
        String sql = "Select * from message where id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                message = new Message(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3));
            }

        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
        return message;

    }
    public void delete (int id){
        Connection connection = connect();
        String sql = "delete from message where id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }

    public boolean update (Message message){
        Connection connection = connect();
        String sql = "update message set message = ?, language = ? where id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,message.getMessage());
            preparedStatement.setString(2, message.getLanguage());
            preparedStatement.setInt(3, message.getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return true;
            }return false;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not update statement",throwables);
        }finally {
            close(connection);
        }
    }



    public void insert(String text, String language) {
        Connection connection = connect();
        String sql = "insert into message(message,language) VALUES ('" + text+"','"+ language +"')";
            Statement insert = null;
            try {
                insert = connection.createStatement();
                insert.execute(sql);

            } catch (SQLException throwables) {
                throw new RuntimeException("Could not create statement", throwables);
            }finally {
                close(connection);
            }
    }

     
    private Connection connect(){
        return databaseAccess.connection();
    }

    private void close(Connection connection){
        databaseAccess.close(connection);
    }
}
