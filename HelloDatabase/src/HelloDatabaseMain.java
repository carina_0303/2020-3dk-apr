
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HelloDatabaseMain {

    public static void main(String[] args) throws DbVendorNotSupportedException {

        String dbVendor = "null";
        if(args.length==1 && args[0].equals("oracle")){
            dbVendor = "oracle";
        }else{
            dbVendor = "mariadb"; //use "postkreis" to get an error
        }

        MessageDataAccessObject messageDao = new MessageDataAccessObject("localhost", "3306", "hello_world", "root", "", dbVendor);
        List<Message> messages = messageDao.readAllMessages();
        //messageDao.insert("Bonjour monde", "fr");
        messages.forEach(System.out::println);
        Message msg = messageDao.read(1);
        System.out.println(msg);
        //messageDao.delete(1);
        Message msg1 = new Message(4,"Ahoj svete","cz");
        messageDao.update(msg1);
        messageDao.readAllMessages();



    }



}
